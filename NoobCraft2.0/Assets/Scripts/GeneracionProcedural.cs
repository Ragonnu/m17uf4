﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class GeneracionProcedural : MonoBehaviour
{
    [Header("Terreno")]
    public float suavidad;
    public float semilla;
    public int alto;
    public int ancho;

    [Header("Cueva")]
    [Range(0, 1)]
    public float modificador;

    [Header("Bloques")]
    public GameObject bloquePiedra;
    public GameObject bloqueHierro;
    public GameObject bloqueDiamante;
    public GameObject fondo;
    public GameObject noob;

    private float[,] mapa;
    private int generaciones = 0;

    void Start()
    {
        Generacion();
    }

    // Update is called once per frame
    void Update()
    {
        Control();
    }

    private void Control()
    {
        int posicion = (ancho / 2) * (generaciones);
        print(posicion);
        if(noob.gameObject.transform.position.x > posicion)
        {
            print("posicion: " + posicion);
            Generacion();
        }
    }

    public void Generacion()
    {
        mapa = GenerarMapa(ancho, alto, true);
        mapa = GenerarTerreno(mapa);
        RellenarMapa(mapa);
        generaciones++;
        print("generado " + generaciones);
    }
    public float[,] GenerarMapa(int ancho, int alto, bool hueco)
    {
        float[,] mapa = new float[ancho * (generaciones + 1), alto];

        for (int x = ancho * generaciones; x < ancho * (generaciones + 1); x++)
        {
            for (int y = 0; y < alto; y++)
            {
                if (hueco) mapa[x, y] = 0;
                else mapa[x, y] = 1;
            }
        }

        return mapa;    
    }

    public float[,] GenerarTerreno(float[,] mapa)
    {
        int alturaPerlin;
        for (int x = ancho * generaciones; x < ancho * (generaciones + 1); x++)
        {
            alturaPerlin = Mathf.RoundToInt(Mathf.PerlinNoise(x / suavidad, semilla) * alto/2);
            //alturaPerlin += alto / 2;
            for (int y = 0; y < alturaPerlin; y++)
            {
                int intCueva = Mathf.RoundToInt(Mathf.PerlinNoise((x * modificador) + semilla, (y * modificador) + semilla));
                if(intCueva == 1) mapa[x, y] = 2;
                else mapa[x, y] = 1;
            }
        }
        return mapa;
    }

    public void RellenarMapa(float[,] mapa)
    {
        for (int x = ancho * generaciones; x < ancho * (generaciones + 1); x++)
        {
            for (int y = 0; y < alto; y++)
            {
                if (mapa[x, y] == 2)
                {
                    Instantiate(fondo, new Vector3(x, y, 2), Quaternion.identity);
                    //tileMap.SetTile(new Vector3Int(x, y, 0), tile1);
                }
                else if (mapa[x, y] == 1)
                {
                    int r = Random.Range(1, 50);
                    
                    if (r > 12 && r < 15) Instantiate(bloqueHierro, new Vector2(x, y), Quaternion.identity);
                    else if(r > 48) Instantiate(bloqueDiamante, new Vector2(x, y), Quaternion.identity);
                    else Instantiate(bloquePiedra, new Vector2(x, y), Quaternion.identity);
                }                    
            }
        }
    }

}
