﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class GeneracionProcedural : MonoBehaviour
{
    [Header("Terreno")]
    public float suavidad;
    public float semilla;
    public int alto;
    public int ancho;

    [Header("Cueva")]
    [Range(0, 1)]
    public float modificador;

    [Header("TileMap")]
    public Tilemap tileMap;
    public TileBase tile1;
    public TileBase tile2;

    private int[,] mapa;

    void Start()
    {
        tileMap.ClearAllTiles();
        mapa = GenerarMapa(ancho, alto, true);
        mapa = GenerarTerreno(mapa);
        RellenarMapa(mapa, tileMap, tile1, tile2);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public int[,] GenerarMapa(int ancho, int alto, bool hueco)
    {
        int[,] mapa = new int[ancho, alto];

        for (int x = 0; x < ancho; x++)
        {
            for (int y = 0; y < alto; y++)
            {
                if (hueco) mapa[x, y] = 0;
                else mapa[x, y] = 1;
            }
        }

        return mapa;    
    }

    public int[,] GenerarTerreno(int[,] mapa)
    {
        int alturaPerlin;
        for (int x = 0; x < ancho; x++)
        {
            alturaPerlin = Mathf.RoundToInt(Mathf.PerlinNoise(x / suavidad, semilla) * alto/2);
            //alturaPerlin += alto / 2;
            for (int y = 0; y < alturaPerlin; y++)
            {
                int intCueva = Mathf.RoundToInt(Mathf.PerlinNoise((x * modificador) + semilla, (y * modificador) + semilla));
                if(intCueva == 1) mapa[x, y] = 2;
                else mapa[x, y] = 1;
            }
        }
        return mapa;
    }

    public void RellenarMapa(int[,] mapa, Tilemap tileMap, TileBase tile1, TileBase tile2)
    {
        for (int x = 0; x < ancho; x++)
        {
            for (int y = 0; y < alto; y++)
            {
                if(mapa[x, y] == 2)
                {

                    tileMap.SetTile(new Vector3Int(x, y, 0), tile1);
                }
                else if(mapa[x, y] == 1) tileMap.SetTile(new Vector3Int(x, y, 0), tile2);
            }
        }
    }

}
