﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneracionPerlin : MonoBehaviour
{
    [Header("Generacion")]
    public int amplitud;
    public float frecuencia;
    public float seedX;
    public float seedY;

    [Header("Mapa")]
    public int ancho;
    public int largo;
    
    
    void Start()
    {
        GetComponent<Terrain>().terrainData = GenerarTerreno(GetComponent<Terrain>().terrainData);
    }

    private TerrainData GenerarTerreno(TerrainData td)
    {
        td.size = new Vector3(ancho, amplitud, largo);
        td.SetHeights(0, 0, GenerarAlturas());
        return td;
    }

    private float[,] GenerarAlturas()
    {
        float[,] alturas = new float[ancho, largo];
        for (int x = 0; x < ancho; x++)
        {
            for (int y = 0; y < largo; y++)
            {
                alturas[x, y] = Mathf.PerlinNoise(x / frecuencia + seedX, y / frecuencia + seedY);
            }
        }
        return alturas;
    }

    void Update()
    {
        
    }
}
